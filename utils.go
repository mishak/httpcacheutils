package httpcacheutils

import (
	"errors"
	"net/http"
	"strconv"
	"time"
)

var ErrCachedResponse = errors.New("cached response")

// AlwaysCache throws away response Cache-Control, Expires, and Pragma headers
func AlwaysCache(transport http.RoundTripper) http.RoundTripper {
	if transport == nil {
		transport = http.DefaultTransport
	}
	return &alwaysCache{transport}
}

// AcceptStale sets request Cache-Control header to max-stale[=<age in seconds>]
func AcceptStale(transport http.RoundTripper, age time.Duration) http.RoundTripper {
	if transport == nil {
		transport = http.DefaultTransport
	}

	value := "max-stale"
	if age > 0 {
		value += "=" + strconv.FormatInt(int64(age.Seconds()), 10)
	}

	return &acceptStale{transport, []string{value}}
}

// ErrorOnCached returns error ErrCachedResponse instead of cached response
func ErrorOnCached(transport http.RoundTripper) http.RoundTripper {
	if transport == nil {
		transport = http.DefaultTransport
	}
	return &errorOnCached{transport}
}

type alwaysCache struct {
	t http.RoundTripper
}

type acceptStale struct {
	t http.RoundTripper
	v []string
}

type errorOnCached struct {
	t http.RoundTripper
}

var (
	headerCacheControl = http.CanonicalHeaderKey("Cache-Control")
	headerExpires      = http.CanonicalHeaderKey("Expires")
	headerPragma       = http.CanonicalHeaderKey("Pragma")

	headerXFromCache = http.CanonicalHeaderKey("X-From-Cache")
)

func (t *alwaysCache) RoundTrip(req *http.Request) (resp *http.Response, err error) {
	resp, err = t.t.RoundTrip(req)
	if err != nil {
		return
	}

	delete(resp.Header, headerCacheControl)
	delete(resp.Header, headerExpires)
	delete(resp.Header, headerPragma)

	return
}

func (t *acceptStale) RoundTrip(req *http.Request) (*http.Response, error) {
	req.Header[headerCacheControl] = t.v[:]
	return t.t.RoundTrip(req)
}

func (t *errorOnCached) RoundTrip(req *http.Request) (res *http.Response, err error) {
	res, err = t.t.RoundTrip(req)
	if err != nil {
		return
	}

	if res.Header[headerXFromCache] != nil {
		res.Body.Close()
		return nil, ErrCachedResponse
	}

	return
}
