# HTTP Cache Utils

Niche library complementing [`github.com/gregjones/httpcache`](https://github.com/gregjones/httpcache).

## Accept Stale Responses

- *I don't care if data is fresh and I would rather not wait.*
- *I want to avoid spamming 3rd party services during testing.*
- *This is the last time I got banned for days because service kept crashing.*

```golang
cache := httpcache.NewTransport(diskcache.New(".http-cache"))
cache.Transport = http.DefaultTransport

// use cached response for at least one hour before asking for a fresh one
http.DefaultTransport = httpcacheutils.AcceptStale(cache, time.Hour)

res, err := http.Get("https://something.very.expensive")
```

## Always Cache Responses

- *Sometimes one server is misconfigured and sometimes you had enough writing exceptions for every other API endpoint.*

```golang
cache := httpcache.NewTransport(diskcache.New(".http-cache"))
cache.Transport = http.DefaultTransport

// ignore caching instructions sent in response
http.DefaultTransport = httpcacheutils.AlwaysCache(cache)

res, err := http.Get("https://something.refusing/to/be/cached")
```

## Error on Cached Response

Useful for short-circuiting code that would be processing cached data.

```golang
cache := httpcache.NewTransport(diskcache.New(".http-cache"))
cache.Transport = http.DefaultTransport

// error instead of returning cached response
http.DefaultTransport = httpcacheutils.ErrorOnCachedResponse(cache)

for {
    res, err := someapi.GetResource(123)
    if errors.Is(err, httpcacheutils.ErrCached) {
        break
    }
}
```
